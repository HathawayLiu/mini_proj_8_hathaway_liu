# Mini Project 8: Command Line Tool with Testing
## Project Description
This project we use Rust to create a command line tool using `Clap` that allows user specifies their arguments. This command line tool also involves several testing function to test the output of the function and see if the false argument tigger the `panic!()` statement in the function. 

## Dataset Strucure
For data processing of this project, I use a simple csv file `grocery.csv` that locates in the `data` folder, where it's a data table that contains the following columns:
```rust
struct GroceryList {
    item: String, // grocery name
    price: f32, // price of the grocery
    types: String, // type of the grocery
    unit_num: String, // unit that the grocery belongs to in the store
}
```
## Function usage
To process the data, I wrote a function that allows user to specify `item`, `price`, and `unit_num` where the output will display the rows in table that correspond to the user-specified `item` and `unit_num` and price that larger than the user-specified `price`. It also includes conditions that allow function to panic if `price` passed in is 0.0, `item` and `unit_num` if the arguments passed in are not exist in the csv.
The function is specified as following: 
```rust
fn filter_table(table: Vec<GroceryList>, price: &str, types: &str, unit_num: &str) -> Result<Vec<GroceryList>, Box<dyn Error>> {
    // Parse the string arguments to their appropriate types
    let price: f32 = price.parse()?;
    if price == 0.0 {
        panic!("The price could not be zero!");
    }
    let types_exist = table.iter().any(|row| row.types == types);
    let unit_num_exist = table.iter().any(|row| row.unit_num == unit_num);
    if !types_exist {
        panic!("The specified types do not exist in the CSV!");
    }
    if !unit_num_exist {
        panic!("The specified unit number does not exist in the CSV!");
    }
    // Filter the table based on the given criteria
    let filtered_table: Vec<GroceryList> = table.into_iter()
        .filter(|row| row.price >= price && row.types == types && row.unit_num == unit_num)
        .collect();

    Ok(filtered_table)
}
```
## Testing
The program includes three testing functions that test the corresponding outputs for correct arguments and if the `panic!()` function is triggered for false arguments. The testing functions are like the following:
```rust
#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_filter() {
        let input_file = "data/grocery.csv";
        let types = "Vegetable";
        let price = "1.5";
        let unit_num = "102";

        let table = load_csv(input_file).unwrap();
        let result_table = filter_table(table, price, types, unit_num).unwrap();

        assert_eq!(result_table.len(), 2);
        assert_eq!(result_table[0].item, "Tomatoes");
        assert_eq!(result_table[0].price, 1.89);
        assert_eq!(result_table[1].item, "Lettuce");
        assert_eq!(result_table[1].price, 1.99);
    }

    #[tokio::test]
    #[should_panic(expected = "The price could not be zero!")]
    async fn test_zero_price() {
        let input_file = "data/grocery.csv";
        let types = "Diary";
        let price = "0";
        let unit_num = "102";

        let table = load_csv(input_file).unwrap();
        let _ = filter_table(table, price, types, unit_num).unwrap();        
    }

    #[tokio::test]
    #[should_panic(expected = "The specified types do not exist in the CSV!")]
    async fn test_not_exist_type() {
        let input_file = "data/grocery.csv";
        let types = "Furniture";
        let price = "12.99";
        let unit_num = "203";

        let table = load_csv(input_file).unwrap();
        let _ = filter_table(table, price, types, unit_num).unwrap();        
    }
}
```
## Detailed Steps
1. run `cargo new <YOUR PROJECT NAME>` to create a new rust project.
2. Go to `main.rs`, start to write your implementation of your own Vector database. You could change the functionality and fields of vector with your own choice. Make sure to add the corresponding dependencies in `Cargo.toml`:
```
csv = "1.1"
clap = "2.33.3"
serde_json = "1.0.113"
serde = { version = "1.0.196", features = ["derive"] }
tokio = { version = "1.37.0", features = ["rt", "rt-multi-thread", "macros"] }
```
Add `println` to display necessary output.
4. When you finish writing, make sure to run `cargo build` and the following command to test if the function works:
```rust
cargo run -- data/grocery.csv -p 1.5 -t Vegetable -u 102
// cargo run -- <YOUR FILE PATH> -p price -t types -u unit_num
```
where the `-p`, `-t`, `-u` are the short expression of `price`, `types` and `unit_num` that are specified in the `main()` function.
The example output will be looked like the following:
```
GroceryList { item: "Tomatoes", price: 1.89, types: "Vegetable", unit_num: "102" }
GroceryList { item: "Lettuce", price: 1.99, types: "Vegetable", unit_num: "102" }
```
5. Use `cargo test` to see if the tests are working.

## Successful output and testing
- Successful testing
![tesing](https://gitlab.com/HathawayLiu/mini_proj_8_hathaway_liu/-/wikis/uploads/5578ba8fd2e9aece22230796cfb8ff7c/Screenshot_2024-03-30_at_7.58.59_PM.png)
- Output Display
![output](https://gitlab.com/HathawayLiu/mini_proj_8_hathaway_liu/-/wikis/uploads/2767d14bea1ccf68fea975cc371778cc/Screenshot_2024-03-30_at_7.59.38_PM.png)

## Reference
- [Writing a CLT tool in Rust](https://medium.com/coderhack-com/writing-a-cli-tool-in-rust-237d7e6417f6)
- [Unit Testing of Rust](https://doc.rust-lang.org/rust-by-example/testing/unit_testing.html)
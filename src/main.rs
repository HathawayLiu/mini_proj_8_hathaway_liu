extern crate clap;
use clap::{App, Arg};
use csv::Reader;
use std::error::Error;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
struct GroceryList {
    item: String,
    price: f32,
    types: String,
    unit_num: String,
}

fn load_csv(input_file: &str) -> Result<Vec<GroceryList>, Box<dyn Error>> {
    let mut grocery_list = Vec::new();
    // Using `from_path` to automatically handle file opening
    let mut reader = Reader::from_path(input_file)?;
    
    for result in reader.deserialize() {
        let record: GroceryList = result?;
        grocery_list.push(record);
    }
    Ok(grocery_list)
}

fn filter_table(table: Vec<GroceryList>, price: &str, types: &str, unit_num: &str) -> Result<Vec<GroceryList>, Box<dyn Error>> {
    // Parse the string arguments to their appropriate types
    let price: f32 = price.parse()?;
    if price == 0.0 {
        panic!("The price could not be zero!");
    }

    let types_exist = table.iter().any(|row| row.types == types);
    let unit_num_exist = table.iter().any(|row| row.unit_num == unit_num);
    
    if !types_exist {
        panic!("The specified types do not exist in the CSV!");
    }
    if !unit_num_exist {
        panic!("The specified unit number does not exist in the CSV!");
    }

    // Filter the table based on the given criteria
    let filtered_table: Vec<GroceryList> = table.into_iter()
        .filter(|row| row.price >= price && row.types == types && row.unit_num == unit_num)
        .collect();

    Ok(filtered_table)
}

fn main() {
    let matches = App::new("CLI tool")
        .version("0.1.0")
        .arg(Arg::with_name("file")                
             .help("Sets input file")    
             .takes_value(true)
             .required(true)
             .index(1))
        .arg(Arg::with_name("price")                
             .help("Filter the csv by the price of items")
             .short("p")
             .long("price")    
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("types")                
             .help("Filter the csv by specific types")
             .short("t")
             .long("types")    
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("unit_num")                
             .help("Filter the csv by Unit number that the items belong to")
             .short("u")
             .long("unit_num")    
             .takes_value(true)
             .required(true))
        .get_matches();

        let input_file = matches.value_of("file").unwrap();
        let price = matches.value_of("price").unwrap();
        let types = matches.value_of("types").unwrap();
        let unit_num = matches.value_of("unit_num").unwrap();

        let table = load_csv(input_file).unwrap();
        let result_table = filter_table(table, price, types, unit_num).unwrap();
        for row in result_table {
            println!("{:?}", row);
        }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_filter() {
        let input_file = "data/grocery.csv";
        let types = "Vegetable";
        let price = "1.5";
        let unit_num = "102";

        let table = load_csv(input_file).unwrap();
        let result_table = filter_table(table, price, types, unit_num).unwrap();

        assert_eq!(result_table.len(), 2);
        assert_eq!(result_table[0].item, "Tomatoes");
        assert_eq!(result_table[0].price, 1.89);
        assert_eq!(result_table[1].item, "Lettuce");
        assert_eq!(result_table[1].price, 1.99);
    }

    #[tokio::test]
    #[should_panic(expected = "The price could not be zero!")]
    async fn test_zero_price() {
        let input_file = "data/grocery.csv";
        let types = "Diary";
        let price = "0";
        let unit_num = "102";

        let table = load_csv(input_file).unwrap();
        let _ = filter_table(table, price, types, unit_num).unwrap();        
    }

    #[tokio::test]
    #[should_panic(expected = "The specified types do not exist in the CSV!")]
    async fn test_not_exist_type() {
        let input_file = "data/grocery.csv";
        let types = "Furniture";
        let price = "12.99";
        let unit_num = "203";

        let table = load_csv(input_file).unwrap();
        let _ = filter_table(table, price, types, unit_num).unwrap();        
    }
}